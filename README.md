# Inhaltsübersicht
- [Inhaltsübersicht](#inhalts-bersicht)
- [Bash](#bash)
  * [for loop](#for-loop)
  * [while loop](#while-loop)
  * [Sudo und Redirection](#sudo-und-redirection)
- [Diverses](#diverses)


# Bash

## for loop

Run command five times

```bash
for i in {1..5}; do COMMAND-HERE; done

for((i=1;i<=10;i+=2)); do echo "Welcome $i times"; done
```
Work on files

```bash
for i in *; do echo $i; done

for i in /etc/*.conf; do cp $i /backup; done
```

## while loop

':' means always 'true'

```bash
while :; do tree .git; date; sleep 1; clear; done
```

```bash
# Alternative bei fehlendem tree command 
while :; do ls -R | grep ":$" | sed -e 's/:$//' -e 's/[^-][^\/]*\//--/g' -e 's/^/   /' -e 's/-/|/'; date; sleep 5; done
```


## Sudo und Redirection
Aufgabe: Die Installation mit dem Paketmanager pacman muss mit sudo ausgeführt werden. Die Ergebnisse werden normalerweise nur auf der Konsole angezeigt. Mit tee sollen die Ergebnisse auch in eine Datei umgeleitet werden.  

```bash
sudo sh -c "pacman -Syu | tee logs/linux-2018-10-14.log"
```

* [Bash redirection](https://wincent.com/wiki/Bash_redirection)
* [Sudo and redirection](https://wincent.com/wiki/Sudo_and_redirection)

# Diverses

* [TOC Generierung für MarkDown](https://ecotrust-canada.github.io/markdown-toc/)